from django.shortcuts import redirect
from django.urls import reverse_lazy
from django.views.generic.detail import DetailView
from django.views.generic.edit import CreateView, DeleteView, UpdateView
from django.views.generic.list import ListView

from django.contrib.auth.mixins import LoginRequiredMixin

from recipes.forms import RatingForm
from recipes.models import Recipe

# try:
#    from recipes.forms import RecipeForm
#    from recipes.models import Recipe
# except Exception:
#    RecipeForm = None
#    Recipe = None


# def log_rating(request, recipe_id):
#     if request.method == "POST":
#         form = RatingForm(request.POST)
#         if form.is_valid():
#             rating = form.save(commit=False)
#             rating.recipe = Recipe.objects.get(pk=recipe_id)
#             rating.save()
#     return redirect("recipe_detail", pk=recipe_id)


# def log_rating(request, recipe_id):
#     if request.method == "POST":
#         form = RatingForm(request.POST)
#     if form.is_valid():
#         try:
#             rating = form.save(commit=False)
#         except Recipe.DoesNotExist:
#             return redirect("recipes_list")
#         rating.recipe = Recipe.objects.get(pk=recipe_id)
#         rating.save()
#     return redirect("recipes_list")


def log_rating(request, recipe_id):
    if request.method == "POST":
        form = RatingForm(request.POST)
    if form.is_valid():
        rating = form.save(commit=False)
        try:
            rating.recipe = Recipe.objects.get(pk=recipe_id)
        except Recipe.DoesNotExist:
            return redirect("recipes_list")
        rating.save()
    return redirect("recipe_detail", pk=recipe_id)


class RecipeListView(LoginRequiredMixin, ListView):
    model = Recipe
    template_name = "recipes/list.html"
    paginate_by = 2


class RecipeDetailView(LoginRequiredMixin, DetailView):
    model = Recipe
    template_name = "recipes/detail.html"

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context["rating_form"] = RatingForm()
        # self.request.GET pis a dictionary, get value out associated with the key "servings"
        # store it in the context dictionary, with the key "servings"
        context["servings"] = self.request.GET.get("servings")
        return context


class RecipeCreateView(LoginRequiredMixin, CreateView):
    model = Recipe
    template_name = "recipes/new.html"
    fields = ["name", "description", "image", "servings"]
    success_url = reverse_lazy("recipes_list")

    def form_valid(self, form):
        form.instance.author = self.request.user
        return super().form_valid(form)


class RecipeUpdateView(LoginRequiredMixin, UpdateView):
    model = Recipe
    template_name = "recipes/edit.html"
    fields = ["name", "description", "image", "servings"]
    success_url = reverse_lazy("recipes_list")


class RecipeDeleteView(LoginRequiredMixin, DeleteView):
    model = Recipe
    template_name = "recipes/delete.html"
    success_url = reverse_lazy("recipes_list")


def create_shopping_item(request):
    # Get the value for the "ingredient_id" from the
    # request.POST dictionary using the "get" method
    ingredient_id = request.POST.get("ingredient_id")
    # Get the specific ingredient from the Ingredient model
    # using the code
    # Ingredient.objects.get(id=the value from the dictionary)
    ingredient = Ingredient.objects.get(id=ingredient_id)
    # Get the current user which is stored in request.user
    user = request.user
    try:
        # Create the new shopping item in the database
        ShoppingItem.objects.create(
            food_item=ingredient.food,
            user=user,
        )
    except IntegrityError:  # Raised if someone tries to add
        pass  # the same food twice, just ignore it

    # Go back to the recipe page with a redirect
    # to the name of the registered recipe detail
    # path with code like this
    # return redirect(
    #     name of the registered recipe detail path,
    #     pk=id of the ingredient's recipe
    # )
    return redirect("recipe_detail", pk=ingredient.recipe.id)
