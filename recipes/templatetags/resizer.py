from django import template

register = template.Library()


def resize_to(ingredient, target):
    print("ingredient:", ingredient)
    print("target:", target)
    # Get the number of servings from the ingredient's
    # recipe using the ingredient.recipe.servings
    # properties
    servings = ingredient.recipe.servings
    # If the servings from the recipe is not None
    #   and the value of target is not None
    if servings is not None and target is not None:
        # try
        try:
            # calculate the ratio of target over
            #   servings
            ratio = int(target) / servings
            # return the ratio multiplied by the
            #   ingredient's amount
            return ingredient.amount * ratio
        # catch a possible error
        except ValueError:
            pass
    # return the original ingredient's amount since
    #   nothing else worked
    return ingredient.amount


register.filter(resize_to)
